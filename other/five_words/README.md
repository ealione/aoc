A Rust port of https://github.com/oisyn/parkerwords/ C++ implementation.

> **Note**
> 
> Cross-link to Golang implementation, which is surprisingly fast https://github.com/yarcat/parkerwords-go


## Description

The algorithm handles words as bitsets stored in a 32-bit integer, where each bit position represents the inclusion of that letter in the word, with 'a' being bit 0, 'b' bit 1, and so forth, up to 26 bits in total. Using a bitwise AND, we can quickly check if two words have overlapping letters, which would then give a non-zero result.

Furthermore, it uses an index to quickly look up a list of words containing a certain letter. By leveraging the fact that the algorithm looks for the letters in a certain order, we only need to store each word in the index once; with it's lowest ordered letter as the index. To determine the order of the letters, the frequency of each letter is recorded and the order of letters is from least to most frequently used letter (using this dataset, it produces the order: "qxjzvfwbkgpmhdcytlnuroisea").

As there are 26 letters in the English alphabet, and we're looking for a list of 5 words, only one letter remains unused. The algorithm is therefore only allowed to skip a single letter.

It basically works as follows:

1. Start with an empty set of letters.
2. Look up which words contains the lowest unused letter
3. For each word in step 2, check whether all its letters are still unused by intersecting it with the current set (using bitwise AND). If this is the case, add it to the set and recursively apply step 2, 3 and 4, until you find a set of 5 words; this is a valid solution.
4. If you have not skipped a letter before, skip the lowest unused letter and redo step 2 again but with the next-lowest unused letter.


This implementation

```
$ cargo build --release

$ ./target/release/parkerwords-rs
[src\main.rs:183] ctx.all_word_bits.len() = 5977
538 solutions written to solutions.txt
Total time:    18646µs
Read:           9114µs
Process:        8724µs
Write:           808µs

$ ./target/release/parkerwords-rs
[src\main.rs:183] ctx.all_word_bits.len() = 5977
538 solutions written to solutions.txt
Total time:    17763µs
Read:           8614µs
Process:        8343µs
Write:           805µs

$ ./target/release/parkerwords-rs
[src\main.rs:183] ctx.all_word_bits.len() = 5977
538 solutions written to solutions.txt
Total time:    18560µs
Read:           8155µs
Process:        9796µs
Write:           609µs
```

Original C++ implementation

```
$ g++ parkerwords.cpp -std=c++20 -O3 -o parkerwords

$ ./parkerwords
538 solutions written to solutions.txt.
Total time: 29999us (0.029999s)
Read:       11998us
Process:    17001us
Write:       1000us

$ ./parkerwords
538 solutions written to solutions.txt.
Total time: 25999us (0.025999s)
Read:       10998us
Process:    14000us
Write:       1001us

$ ./parkerwords
538 solutions written to solutions.txt.
Total time: 27000us (0.027s)
Read:       10998us
Process:    15004us
Write:        998us

$ ./parkerwords
538 solutions written to solutions.txt.
Total time: 28020us (0.02802s)
Read:       10984us
Process:    17036us
Write:          0us
```
